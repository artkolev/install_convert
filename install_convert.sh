#!/bin/sh
echo "Установка макроса конвертации";
echo "Первоначальный вызов soffice для создания директории конфига пользователя";
soffice --invisible --nofirststartwizard --headless --norestore "macro:///Standard.Module1.Main()";
echo "Создание ссылки на директорию конфигурации";
ln -s ~/.config/libreoffice/4/user/basic/ ./libre_config;
echo "Копирование макроса в директорию назначения";
cp ./ModuleConvert.xba ./libre_config/Standard/ModuleConvert.xba;
cd ./libre_config/Standard;
if grep --quiet ModuleConvert script.xlb ; then
	echo "Кофиг уже имеет запись о макросе";
else
	echo "Копирование сущетвующего конфига";
	cp ./script.xlb ./script.xlb.sav;
	echo "Добавление записи о новом макросе";
	head -n -1 ./script.xlb.sav > ./script.xlb;
	echo ' <library:element library:name="ModuleConvert"/>' >> ./script.xlb;
	tail -n 1  ./script.xlb.sav >> ./script.xlb;
fi;
cd ../..;
echo "Готово";
echo "Использование: soffice --invisible --nofirststartwizard --headless --norestore \"macro:///Standard.ModuleConvert.Main(/home/test/public_html/test.xls, /tmp)\"\n";
